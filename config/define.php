<?php

$GLOBALS['debugger']['initiated'] = false;


if (php_sapi_name() === 'cli') {
    $cliConfig = require_once __DIR__ . "/cli.php";

    $cliConfigExtensionPath = dirname(__DIR__, 4) . "/debugger__cli.php";
    if (file_exists($cliConfigExtensionPath)) {
        $cliConfig = debugger__mergeCliConfigs($cliConfig, require_once $cliConfigExtensionPath);
    }

    define('DEBUGGER__LABEL_COLOR', $cliConfig['color']['label']);
    define('DEBUGGER__SUCCESS_LABEL_COLOR', $cliConfig['color']['success_label']);
    define('DEBUGGER__FAILURE_LABEL_COLOR', $cliConfig['color']['failure_label']);
    define('DEBUGGER__POINTER_COLOR', $cliConfig['color']['pointer']);
    define('DEBUGGER__COLOR_END', $cliConfig['color']['end']);
    define('DEBUGGER__FUNCTION_TITLE_COLOR', $cliConfig['color']['functionTitle']);
    define('DEBUGGER__FUNCTION_ARGS_COLOR', $cliConfig['color']['functionArgs']);
    define('DEBUGGER__DIR_PATH_COLOR', $cliConfig['color']['dirPath']);
    define('DEBUGGER__VAR_TYPE_COLOR', $cliConfig['color']['varType']);
    define('DEBUGGER__OUTPUT_NUM_COLOR', $cliConfig['color']['outputNum']);
} else {
    define('DEBUGGER__LABEL_COLOR', "<div class='_debugger__label'>");
    define('DEBUGGER__SUCCESS_LABEL_COLOR', "<div class='_debugger__label'>");
    define('DEBUGGER__FAILURE_LABEL_COLOR', "<div class='_debugger__label'>");
    define('DEBUGGER__POINTER_COLOR', "<div class='_debugger__pointer'>");
    define('DEBUGGER__COLOR_END', "</div>");
    define('DEBUGGER__FUNCTION_TITLE_COLOR', "<div class='_debugger__function-title'>");
    define('DEBUGGER__FUNCTION_ARGS_COLOR', "<div class='_debugger__function-args'>");
    define('DEBUGGER__DIR_PATH_COLOR', "<div class='_debugger__dir-path'>");
    define('DEBUGGER__VAR_TYPE_COLOR', "<div class='_debugger__var-type'>");
    define('DEBUGGER__OUTPUT_NUM_COLOR', "<div class='_debugger__output-num'>");
}