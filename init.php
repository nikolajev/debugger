<?php

require_once __DIR__ . "/src/debugger_functions.php";
require_once __DIR__ . "/config/define.php";

require_once __DIR__ . "/src/show.php";
require_once __DIR__ . "/src/showln.php";
require_once __DIR__ . "/src/debug.php";
require_once __DIR__ . "/src/trace.php";
require_once __DIR__ . "/src/success.php";
require_once __DIR__ . "/src/failure.php";