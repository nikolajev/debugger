<?php
// @todo Check if function exists
function failure(?string $message = null, bool $displayCaller = true)
{
    call_user_func('debugger__success', $message, $displayCaller, 'FAILURE', DEBUGGER__FAILURE_LABEL_COLOR);
}