<?php

// @todo Specify array slice params
function debugger__trace(int $offset = 1, int $length = null)
{
    $pointerColor = DEBUGGER__POINTER_COLOR;
    $functionTitleColor = DEBUGGER__FUNCTION_TITLE_COLOR;
    $functionArgsColor = DEBUGGER__FUNCTION_ARGS_COLOR;
    $dirPathColor = DEBUGGER__DIR_PATH_COLOR;
    $colorEnd = DEBUGGER__COLOR_END;


    // @todo Update
    $debug_backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

    $debug_backtrace = array_slice($debug_backtrace, $offset, $length);
    
    foreach (array_reverse($debug_backtrace) as $item) {
        //$argString = $functionArgsColor . debugger__prepareArgumentString($item['args']) . $colorEnd;
        $argString = null;
        
        $fileTitle = basename($item['file']);
        $dirPath = dirname($item['file']);

        $backtrace[] = "$pointerColor $fileTitle:{$item['line']} $colorEnd $functionTitleColor{$item['function']}$colorEnd($argString) $dirPathColor$dirPath$colorEnd";
    }

    call_user_func_array('debugger__show', [$backtrace, false, 'TRACE', false]);
}

// @todo Specify array slice params
function trace(int $offset = 1, int $length = null)
{
    call_user_func('debugger__trace', $offset, $length);
}

function traceexit(int $offset = 1, int $length = null)
{
    call_user_func('debugger__trace', $offset, $length);
    debugger__exit();
}