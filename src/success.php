<?php

function debugger__success(?string $message = null, bool $displayCaller = true, string $labelMessage = 'SUCCESS', string $labelColor = DEBUGGER__SUCCESS_LABEL_COLOR)
{
    $dirPathColor = DEBUGGER__DIR_PATH_COLOR;
    $colorEnd = DEBUGGER__COLOR_END;

    // @todo DRY
    $debug_backtrace = debug_backtrace();
    $caller = $debug_backtrace[1];

    $fileTitle = basename($caller['file']);
    $dirPath = dirname($caller['file']);
    echo "$labelColor $labelMessage " . DEBUGGER__COLOR_END;

    if ($displayCaller) {
        echo " $dirPathColor$dirPath/$fileTitle:{$caller['line']}$colorEnd";
    }
    
    if(!empty($message)){
        echo PHP_EOL . $message;   
    }
    
    echo PHP_EOL;;
}

// @todo Check if function exists
function success(?string $message = null, bool $displayCaller = true)
{
    call_user_func('debugger__success', $message, $displayCaller);
}
