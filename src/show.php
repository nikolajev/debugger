<?php

function debugger__show(array $args, bool $showLabel = true, ?string $anotherLabelToShow = null, bool $displayCaller = true)
{
    $dirPathColor = DEBUGGER__DIR_PATH_COLOR;
    $colorEnd = DEBUGGER__COLOR_END;
    
    $label = $showLabel ? 'SHOW' : $anotherLabelToShow;

    $options = $label === null ? [] : [
        'label' => $label,
    ];

    $debug_backtrace = debug_backtrace();
    $caller = $debug_backtrace[1];
    
    $fileTitle = basename($caller['file']);
    $dirPath = dirname($caller['file']);


    $additionalOptions = $displayCaller ? [
        'caller' => "$dirPathColor$dirPath/$fileTitle:{$caller['line']}$colorEnd",
        'simple_caller_reference' => true,
    ]: [];
    
    debugger__printArgs(
        $args,
        "\$i\$arg" . PHP_EOL,
        function ($arg) {

            if (is_array($arg)) {
                return json_encode($arg, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
            }

            if (is_object($arg)) {
                ob_start();
                print_r($arg);
                $print_r = ob_get_clean();
                return $print_r;
            }
        },
        
        array_merge(
            debugger__prepareLabelOptionArray('SHOW', $showLabel, $anotherLabelToShow),
            $additionalOptions
        )
    );
}

function show()
{
    call_user_func_array('debugger__show', [func_get_args()]);
}

function showexit()
{
    call_user_func_array('debugger__show', [func_get_args()]);
    debugger__exit();
}