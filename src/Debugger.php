<?php

namespace Nikolajev\Debugger;

class Debugger
{
    public static function silent(bool $isSilent = true)
    {
        $GLOBALS['nikolajev']['debugger']['silent'] = $isSilent;
    }

    public static function isSilent()
    {
        return $GLOBALS['nikolajev']['debugger']['silent'] ?? false;
    }

}