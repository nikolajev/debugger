<?php

function debugger__showln(array $args, bool $showLabel = true, ?string $anotherLabelToShow = null)
{
    // @todo DRY
    $debug_backtrace = debug_backtrace();
    $caller = $debug_backtrace[2];

    $fileTitle = basename($caller['file']);
    $dirPath = dirname($caller['file']);
    
    
    $options = array_merge(debugger__prepareLabelOptionArray('SHOW ONE LINE', $showLabel, $anotherLabelToShow), [
        'quoted_strings' => true,
        'simple_caller_reference' => true,
        'caller' => DEBUGGER__DIR_PATH_COLOR . "$dirPath/$fileTitle:{$caller['line']}" . DEBUGGER__COLOR_END,
    ]);


    debugger__printArgs($args, "\$i \$arg" . PHP_EOL, function ($arg) {
        if (is_array($arg)) {
            return json_encode($arg);
        }

        if (is_object($arg)) {
            ob_start();
            print_r($arg);
            $print_r = ob_get_clean();
            // Remove linebreaks, remove extra whitespaces from a string
            return preg_replace('/\s+/', ' ', str_replace(["\t", "\r", PHP_EOL], '', $print_r));
        }
    }, $options);
}

function showln()
{
    call_user_func_array('debugger__showln', [func_get_args()]);
}

function showlnexit()
{
    call_user_func_array('showln', func_get_args());
    debugger__exit();
}