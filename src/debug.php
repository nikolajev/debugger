<?php

function debugger__dump()
{
    $pointerColor = DEBUGGER__POINTER_COLOR;
    $dirPathColor = DEBUGGER__DIR_PATH_COLOR;
    $colorEnd = DEBUGGER__COLOR_END;

    $debug_backtrace = debug_backtrace();
    $caller = $debug_backtrace[1];

    $fileTitle = basename($caller['file']);
    $dirPath = dirname($caller['file']);


    debugger__printArgs(func_get_args(), "\$i(\$type) \$arg" . PHP_EOL, function ($arg) {
        if (is_array($arg) || is_object($arg)) {
            ob_start();
            var_dump($arg);
            $var_dump = ob_get_clean();
            if (php_sapi_name() === 'cli') {
                // Remove first line with trace path
                return $var_dump;
                //return substr($var_dump, strpos($var_dump, PHP_EOL) + 1);
            }

            return $var_dump;
        }
        
    }, [
        'caller' => "$pointerColor $fileTitle:{$caller['line']} $colorEnd $dirPathColor$dirPath$colorEnd",
        'label' => 'DEBUG',
    ]);
}

function dumpexit()
{
    call_user_func_array('debugger__dump', func_get_args());
    debugger__exit();
}


if (!function_exists('dump')) {
    function dump()
    {
        call_user_func_array('debugger__dump', func_get_args());
    }
}

if (!function_exists('debug')) {
    function debug()
    {
        call_user_func_array('debugger__dump', func_get_args());
    }

    function debugexit()
    {
        call_user_func_array('dumpexit', func_get_args());
    }
}